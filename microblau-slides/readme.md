# eZ Platform EE Landing Page Management Introduction

---

## Agenda

- Introduction
- Creating layouts
- Creating blocks
- Form builder
- Import / export
- Future, upcoming changes

---

## Introduction

- An extension to eZ Platform
- Part of the eZ Platform enterprise edition
- Installed in the same way as eZ Platform
  - Proprietary bits from alternative composer repository
  - You need tokens for access

---

## Introduction

- Functionalities
  - Editing, managing, scheduling
- Note - Always use clean install for projects!
- Demo env: https://janit.ezstudiodemo.com/ez

---

# Creating layouts

- <a href="https://doc.ezplatform.com/en/1.12/cookbook/creating_landing_page_layouts_(enterprise)/">Docs: Creating landing page layouts</a>

--

## Creating layouts

- Storage:
  - Content & structure stored in a field type
  - Template building in Twig
  - Configuration in YAML

- Begin by importing layouts in `config.yml`:

```
    - { resource: layouts.yml }
```

--

## Creating layouts

- The root element is

```
ez_systems_landing_page_field_type:
```

- You can define blocks and layouts:

```
layouts:
  sidebar:
    identifier: sidebar
    name: With sidebar
    description: Main section with sidebar on the right
    thumbnail: bundles/app/img/layouts/two_columns.png
    template: layouts/two_columns.html.twig
    zones:
      first:
        name: First zone
      second:
        name: Second zone
```

--

## Creating layouts

- Clear caches if not available
- Create template:
  - app/Resources/views/layouts/two_columns.html.twig
- Download image:
 - <a href="https://raw.githubusercontent.com/janit/ez-platform-starter/master/src/AppBundle/Resources/public/img/layouts/two_columns.png">two_columns.png</a> 
- Link assets:

```
./app/console assets:install --symlink
```

--

## Creating layouts

- Layout uses standard Twig with HTML data attributes:

```
<div data-studio-zones-container>
    <main data-studio-zone="{{ zones[0].id }}"></main>
    <aside data-studio-zone="{{ zones[1].id }}"></aside>
</div>
```
- Upload a new image to the media library
- Go to page tab
- Create new page and drop in an embed (for the image)
- View page and see what is sent:

```
{{dump()}}
```

--

## Creating

- Blocks need to be looped and individual blocks rendered with the `ez_block:renderBlockAction` controller:

```
<div data-studio-zones-container>
    <main data-studio-zone="{{ zones[0].id }}">
        {% if zones[0].blocks %}
            {% for block in zones[0].blocks %}
                <div class="landing-page__block block_{{ block.type }}">
                    {{ render_esi(controller('ez_block:renderBlockAction', {
                        'contentId': contentInfo.id,
                        'blockId': block.id,
                        'versionNo': versionInfo.versionNo
                    })) }}
                </div>
            {% endfor %}
        {% endif %}
    </main>
    <aside data-studio-zone="{{ zones[1].id }}">
        {% if zones[1].blocks %}
            {% for block in zones[1].blocks %}
                <div class="landing-page__block block_{{ block.type }}">
                    {{ render_esi(controller('ez_block:renderBlockAction', {
                        'contentId': contentInfo.id,
                        'blockId': block.id,
                        'versionNo': versionInfo.versionNo
                    })) }}
                </div>
            {% endfor %}
        {% endif %}
    </aside>
</div>
```

--

## Creating layouts

- Notes
  - Layout design is completely open, no layout or JS framework is enforced
  - Currently you can not change from layout to another
    - Should be possible in early v2.x releases
  - Debugging can be difficult, always remember the data attributes need to be in place for the front end to attach

---

# Creating blocks

- <a href="https://doc.ezplatform.com/en/1.12/cookbook/creating_landing_page_blocks_(enterprise)/">Docs: Creating landing page blocks</a>

--

## Creating blocks

- Blocks are similar to content
- Configuration
  - Template
  - Attributes
- Templates (Twig)
  - Can have many views

--

## Creating blocks

- You can define one or more view for blocks
- To add a block view to a standard block, edit `layouts.yml`:

```
blocks:
  embed:
    views:
      default:
        template: blocks/default.html.twig
        name: Default
      custom_embed:
        template: blocks/custom_embed.html.twig
        name: Custom Embed
```

- Create templates in `blocks/`
- Create new block, selectt and dump in custom_embed.html.twig

--

## Create blocks

- New blocks can be created in PHP or via `layouts.yml`:

```
youtube_video:
  initialize: true
  name: Youtube Video
  category: default
  thumbnail: bundles/app/img/blocks/youtube.png
  views:
    default:
      template: blocks/youtube.html.twig
      name: Default view
  attributes:
    video_id: string
```

- Individual attributes allow to pass in different attributes:
  - integer, string, url, text, embed, select, multiple, radio

--

## Create blocks

- To get attributes need to be registered in `services.yml`:

```
services:
  ez_systems.landing_page.block.youtube_video:
    class: AppBundle\Block\YoutubeVideo
    tags:
      - { name: landing_page_field_type.block_type, alias: youtube_video }

# if needed
#    arguments:
#      - '@ezpublish.landing_page.block_definition_factory'
#      - '@ezpublish.api.repository'...
```

- view template and `dump()` in template

---

## Form builder

- <a href="https://doc.ezplatform.com/en/1.12/guide/extending_ez_platform_ui/#extending-the-form-builder">Docs: Extending the form builder</a>

--

## Form builder

- Build dynamic form blocks
- Collect form fields
- Integrate to other systems with events
- Add new field types

---

## Import / Export

- Require Kaliop Migrations bundle

```
composer req kaliop/ezmigrationbundle
```

-- Require in kernel:

```
new \Kaliop\eZMigrationBundle\EzMigrationBundle()
```

-- Run command to verify install:

```
php app/console kaliop:migrations
```

--

## Import / Export

- <a href="https://github.com/kaliop-uk/ezmigrationbundle">Kaliop migrations</a> can be used to import content structures, content, etc.
- You can also export content, including landing pages:

```
./app/console kaliop:migration:generate \
    --type=content \
    --match-type=content_id \
    --match-value=52 \
    AppBundle
```

- After that observe the content structure

---

## Future

- eZ Platform v2.0.0 will launch this week
  - The change in UI layer is the major focus
- Landing Page management in v2.0.0 is from v1.13.0
- Landing page field type was overhauled internally in 2017
- Changes will be coming mostly in the UI
  - Scheduled to land in v2.1.0 or v2.2.0
