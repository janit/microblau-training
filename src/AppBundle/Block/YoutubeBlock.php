<?php

namespace AppBundle\Block;

use EzSystems\LandingPageFieldTypeBundle\FieldType\LandingPage\Definition\BlockAttributeDefinitionFactory;
use EzSystems\LandingPageFieldTypeBundle\FieldType\LandingPage\Model\BlockValue;
use EzSystems\LandingPageFieldTypeBundle\FieldType\LandingPage\Model\ConfigurableBlockType;

class YoutubeBlock extends ConfigurableBlockType
{


    public function getTemplateParameters(BlockValue $blockValue)
    {

        $attributes = $blockValue->getAttributes();

        $attributes['random'] = rand(1,10000);

        return $attributes;
    }

}